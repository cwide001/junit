import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Assert {

    @Test
    void testRelationShipStatus() {
        String relationshipStatusA = new String ("Married");
        String relationshipStatusB = new String ("Married");
        assertEquals(relationshipStatusA, relationshipStatusB);
    }
    @Test
    public void city() {
        String cityA = "Menifee";
        String cityB ="Menifee";
        assertSame(cityA, cityB);
    }
    @Test
    public void testAge() {
        int ageA = 19;
        int ageB = 18;
        assertNotSame(ageA, ageB);
    }
    @Test
    public void testCoSocialMedia() {
        String socialMediaA = null;
        String socialMediaB = "Instagram";
        assertNotNull(socialMediaB);
        assertNull(socialMediaA);
    }
    @Test
    public void testPriorities() {
        String[] priorityA = {"eat", "sleep", "pray"};
        String[] priorityB =  {"eat", "sleep", "pray"};
        assertArrayEquals(priorityA, priorityB);
    }
    @Test
    public void checkHeight() {
        int heightA = 6;
        int heightB = 5;
        assertFalse(heightA > heightB);
        assertTrue (heightB > heightA);
    }

}